---
layout: post
toc: true
---

### what is swap ?
* Linux kernel's memory management unit provides and indirection to physical memory 

{% digraph %}
rankdir=LR
node[shape=box]
process2 -> p2
p1 [label=<
<TABLE>
  <TR><TD port="f0">mem:0</TD></TR>
  <TR><TD port="f1">mem:1</TD> </TR>
  <TR><TD>....</TD> </TR>
  <TR><TD>mem:infinite</TD> </TR>
</TABLE>
>];


process1 -> p1:f0 [label="I need 2mb of memory"]
process1 -> p1:f1

p1:f0 -> Mem[image="ram.jpg", ]
p1:f1 -> "swap on disk"

# somenode [width=0.25 height=0.25 fixedsize=true image="ram.jpg", label=""];
{% enddigraph %}


### How to configure swap ?


### How to configure file based swap 


### How to configure zfs pool as swap 


### How to see swap usage 


### what is vm.swappiness ??


### Fields of `grep -i swap /proc/meminfo`


### how kernel chooses pages to be swapped ? 
* Only anon memory is swapped 
  * anon memory is anonymous memory..
  * in a process when a code says `int value=123` the memory allocated to this variable is called `anon memory`
* non-anon == file pages can be flushed to disk
* buffers are taken for granted....managed by kernel for faster access of files/dirs which kernel thinks apps mau need it

### why buffer memory are never swapped ???
